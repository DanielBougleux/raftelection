package raft

import (
	"labrpc"
	"math/rand"
	"sync"
	"time"
)

type ApplyMsg struct {
	Index       int
	Command     interface{}
	UseSnapshot bool
	Snapshot    []byte
}

type Raft struct {
	mu        sync.Mutex          // Lock to protect shared access to this peer's state
	peers     []*labrpc.ClientEnd // RPC end points of all peers
	persister *Persister          // Object to hold this peer's persisted state
	me        int                 // this peer's index into peers[]

	currentTerm int           //comeca em 0 e aumenta de acordo com as eleicoes
	votedFor    int           //inicia em -1, indica em quem votou
	estado      string        //estado do nó: Seguidor, Candidato ou Lider
	timeout     time.Duration //Duracao do timeout do node
	votosRecebidos   int      //indica quantos votos um candidato recebeu

	viraLider       bool  //se deve se tornar lider
	heartbeat       bool //se chegou uma heartbeat
}

// return currentTerm and whether this server
// believes it is the leader.
func (rf *Raft) GetState() (int, bool) {

	//coloca o termo atual do nó na variável Term
	var Term int = rf.currentTerm
	var isleader bool

	//verifica se o nó é o líder e atualiza a variável isleader de acordo
	switch rf.estado {
	case "Lider":
		isleader = true
	case "Candidato", "Seguidor":
		isleader = false
	}
	return Term, isleader
}

// save Raft's persistent state to stable storage,
// where it can later be retrieved after a crash and restart.
func (rf *Raft) persist() {
	// Your code here (2C).
}

// restore previously persisted state.
func (rf *Raft) readPersist(data []byte) {
	if data == nil || len(data) < 1 { // bootstrap without any state?
		return
	}
}

//estrutura que guarda informações sobre o candidato para a rpc request vote
type RequestVoteArgs struct {
	//o termo e o id do candidato para enviar no pedido do voto
	Term        int
	CandidateID int
}

//estrutura que guarda a resposta da rpc request vote
type RequestVoteReply struct {
	//o termo do nó que recebeu o vote request
	Term        int
	//se o nó votou ou não no candidato
	VoteGranted bool
}

//=======================================================================
//====================Novas estruturas e funções=========================
//=======================================================================

//estrutura que encapsula o id do termo para envio na RPC AppendEntries
type Termo struct {
	TermoID int //Id do termo
}

//Torna o node um seguidor de um termo
func (rf *Raft) turnFollower(Termo int) {
	//atuliza o termo
	rf.currentTerm = Termo
	//reinicializa a variavel de voto
	rf.votedFor = -1
	//muda o estado pra seguidor
	rf.estado = "Seguidor"
}

//Torna o node um candidato
func (rf *Raft) turnCandidate() {
	
	//torna o nó um candidato de um novo termo
	rf.currentTerm = rf.currentTerm + 1
	//vota em si mesmo
	rf.votedFor = rf.me
	//muda o estado para candidato
	rf.estado = "Candidato"
	//contagem de votos começa em 1 pois o nó vota em si mesmo
	rf.votosRecebidos = 1
}

//Torna o node um lider
func (rf *Raft) turnLeader() {
	//reinicia a variável do voto
	rf.votedFor = -1
	//muda o estado para líder
	rf.estado = "Lider"
}

//função que implementa a interface do RPC para heartbeats
func (rf *Raft) AppendEntries(args *Termo, reply *Termo) {
	
	//Se o termo do nó que se considera líder está desatualizado,
	//retorna o termo atualizado
	if args.TermoID < rf.currentTerm {
		reply.TermoID = rf.currentTerm
		return
	}

	//trava a estrutura para tratar concorrência
	rf.mu.Lock()

	//se o termo do líder está atualizado, recebe o heartbeat
	rf.heartbeat = true

	//retorna uma confirmação que o termo está correto
	reply.TermoID = args.TermoID

	//se o termo do seguidor está desatualizado, 
	//torna-se um seguidor do termo atualizado
	if rf.currentTerm < args.TermoID {
		rf.turnFollower(args.TermoID)
	}

	//destrava a estrutura
	rf.mu.Unlock()
}

//função que faz a RPC do appendentries e trata o resultado
//implementado em uma função separada para que possa ser utilizada como uma rotina go
func (rf *Raft) sendAppendEntries(node int, args *Termo, reply *Termo) bool {

	//chama a RPC do heartbeat e aguarda o resultado
	success := rf.peers[node].Call("Raft.AppendEntries", args, reply)

	//se a chamada RPC foi executada com sucesso
	if success {

		//se o nó deixou de ser lider durante a execução do RPC, apenas retorna
		switch rf.estado {
		case "Candidato", "Seguidor":
			return success
		}

		//Se o nó que recebeu o heartbeat possuir um termo maior, o líder vira seguidor desse termo
		if rf.currentTerm < reply.TermoID {
			rf.mu.Lock()
			rf.turnFollower(reply.TermoID)
			rf.mu.Unlock()
		}
	}
	return success
}

//=======================================================================
//=======================================================================

//função que implementa a interface do RPC para recebimento de requestvotes
func (rf *Raft) RequestVote(args *RequestVoteArgs, reply *RequestVoteReply) {

	//trava a estrutura do nó para tratamento de concorrência
	rf.mu.Lock()
	if rf.currentTerm < args.Term {
		rf.turnFollower(args.Term)
	}
	//se o Termo do candidato for menor que o Termo do receptor
	if args.Term < rf.currentTerm {
		//o candidato não pode receber o voto
		reply.VoteGranted = false
		//se o Termo está correto e o receptor ainda não votou
	} else if rf.votedFor == -1 {
		//o receptor vota pelo candidato
		reply.VoteGranted = true
		rf.votedFor = args.CandidateID
	}
	//retorna o Termo atual do receptor
	reply.Term = rf.currentTerm
	//destrava a estrutura
	rf.mu.Unlock()
}

//função que faz a RPC do requestvote e trata o resultado
//implementado em uma função separada para que possa ser utilizada como uma rotina go
func (rf *Raft) sendRequestVote(node int, args *RequestVoteArgs, reply *RequestVoteReply) bool {

	//faz a RPC de requisição de voto e guarda o resultado
	success := rf.peers[node].Call("Raft.RequestVote", args, reply)

	//Trata concorrência
	rf.mu.Lock()

	//se o RPC foi bem sucedido
	if success {

		//Se nao for candidato, nao precisa fazer nada
		switch rf.estado {
		case "Lider", "Seguidor":
			return success
		}

		//Se o termo atual estiver desatualizado, deixa de ser candidato
		if rf.currentTerm < reply.Term {
			rf.turnFollower(reply.Term)
			rf.mu.Unlock()
			return success
		}

		//contagem de votos
		if reply.VoteGranted {
			rf.votosRecebidos++
			//Se ja recebeu mais da metade de votos, avisa que virou lider
			if rf.votosRecebidos == len(rf.peers)/2+1 {
				rf.viraLider = true
			}
		}
	}
	//destrava a estrutura e retorna
	rf.mu.Unlock()
	return success
}

func (rf *Raft) Start(command interface{}) (int, int, bool) {
	index := -1
	Term := -1
	isLeader := true
	return index, Term, isLeader
}

func (rf *Raft) Kill() {
	// Your code here, if desired.
}

func Make(peers []*labrpc.ClientEnd, me int,
	persister *Persister, applyCh chan ApplyMsg) *Raft {

	//inicializa as variáveis da estrutura
	rf := &Raft{}
	rf.peers = peers
	rf.persister = persister
	rf.me = me
	rf.currentTerm = 0
	rf.votedFor = -1
	rf.estado = "Seguidor"
	rf.heartbeat = false
	rf.viraLider = false
	
	//inicia uma rotina paralela que cuida do ciclo de vida de um nó do raft
	go func() {

		//contador de intervalos
		var intervalos int8
		//Infinite loop
		for {
			//verifica o estado do nó
			switch rf.estado {
			//se o nó é o líder
			case "Lider":
				//envia as heartbeats
				//encapsula o termo do líder
				args := &Termo{TermoID: rf.currentTerm}
				//para cada nó no raft
				for i := range rf.peers {
					//se o nó não é o próprio lider
					if i != rf.me {
						//cria goroutines que envia a heartbeat para cada nó do raft em paralelo
						go func(i int) {
							//cria uma estrutura para guardar a resposta
							reply := &Termo{}
							//chama a função que gerencia a RPC
							rf.sendAppendEntries(i, args, reply)
					}(i)
					}
				}
				//dorme por 100 ms
				time.Sleep(100 * time.Millisecond)
			//se o nó é um candidato
			case "Candidato":
				//define uma duração aleatória para o tempo limite da eleição
				rf.timeout = time.Duration(rand.Intn(250)+250) * time.Millisecond
				//inicializa a variável que conta os pedaços de timeout
				intervalos = 1
				//inicia a eleição
				//encapsula informações do candidato na estrutura
				args := &RequestVoteArgs{
					Term:        rf.currentTerm,
					CandidateID: rf.me,
				}
				//para cada nó no raft
				for i := range rf.peers {
					//se o nó não é o próprio candidato
					if i != rf.me {
						//cria uma goroutine que envia a requisição para o nó em paralelo
						go func(i int) {
							//cria a estrutura que encapsulará a resposta
							reply := &RequestVoteReply{}
							//faz a chamada da função que enviará e tratará a requisição
							rf.sendRequestVote(i, args, reply)
						}(i)
					}
				}
				//espera o resultado
				for {
					//dorme por um décimo do tempo limite
					time.Sleep(rf.timeout / time.Duration(10))
					if rf.heartbeat { //se receber um heartbeat de outro lider com Termo maior vira follower desse termo
						rf.mu.Lock()
						rf.turnFollower(rf.currentTerm)
						//reinicia as variáveis de controle
						rf.heartbeat = false
						rf.viraLider = false
						rf.mu.Unlock()
						break
					} else if rf.viraLider { //se conseguir a maioria dos votos vira lider
						rf.mu.Lock()
						rf.turnLeader()
						//reinicia as variáveis de controle
						rf.viraLider = false
						rf.heartbeat = false
						rf.mu.Unlock()
						break
					} else if intervalos >= 10 { //se o tempo acabar reinicia a eleição
						rf.mu.Lock()
						rf.turnCandidate()
						rf.mu.Unlock()
						break
					}
					//incrementa a quantidade de intervalos passados
					intervalos++
				}
			//se o nó é um seguidor
			case "Seguidor":
				//estima tempo limite aleatório para o recebimento da heartbeat
				rf.timeout = time.Duration(rand.Intn(250)+250) * time.Millisecond
				//inicializa a variável que conta os pedaços de timeout
				intervalos = 1
				//espera a heartbeat
				for {
					//dorme por um décimo do tempo limite
					time.Sleep(rf.timeout / time.Duration(10))
					//se receber segue a execução
					if rf.heartbeat {
						rf.mu.Lock()
						rf.heartbeat = false
						rf.mu.Unlock()
						break
					} else if intervalos >= 10 { //Se deu timeout
						//se torna candidato
						rf.mu.Lock()
						rf.turnCandidate()
						rf.heartbeat = false
						rf.mu.Unlock()
						break
					}
					//incrementa a quantidade de intervalos passados
					intervalos++
				}
			}
		}
	}()
	// initialize from state persisted before a crash
	rf.readPersist(persister.ReadRaftState())

	return rf
}